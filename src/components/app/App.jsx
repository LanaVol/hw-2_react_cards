import React from "react";
import { Title } from "../title/Title.jsx";
import Cards from "../cards/Cards.jsx";
import { peopleList } from "../../data/peopleList";
import "../cards/Cards.css";

function App() {
  return (
    <>
      <Title />
      <div className="cards">
        <Cards data={peopleList} />
      </div>
    </>
  );
}

export default App;
