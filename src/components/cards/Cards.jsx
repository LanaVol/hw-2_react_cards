import React from "react";
import { Card } from "../card/Card.jsx";

function Cards({ data: { results } }) {
  const keyNum = Math.random().toFixed(1);
  return Array.isArray(results) ? (
    results.map((element, index) => {
      return <Card person={element} key={keyNum + index} />;
    })
  ) : (
    <h2>Error</h2>
  );
}

export default Cards;
