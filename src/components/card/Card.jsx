import React from "react";
import { Button } from "../button/Button.jsx";
import "./Card.css";

export const Card = ({
  person: {
    name,
    height,
    mass,
    hair_color,
    skin_color,
    eye_color,
    birth_year,
    gender,
    url,
  },
}) => {
  return (
    <ul className="card">
      <li className="card__title">{name}</li>
      <li className="card__img"></li>
      <li>
        Height: <span>{height}</span>
      </li>
      <li>
        Mass: <span>{mass}</span>
      </li>
      <li>
        Hair color: <span>{hair_color}</span>
      </li>
      <li>
        Skin color: <span>{skin_color}</span>
      </li>
      <li>
        Eye color: <span>{eye_color}</span>
      </li>
      <li>
        Birth year: <span>{birth_year}</span>
      </li>
      <li>
        Gender: <span>{gender}</span>
      </li>
      <li>
        <Button url={url} />
      </li>
    </ul>
  );
};
