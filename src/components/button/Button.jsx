import React from "react";
import "./Button.css";

export const Button = ({ url }) => {
  return (
    <button type="button">
      <a href={url} target="_blank">
        Show info about hero
      </a>
    </button>
  );
};
